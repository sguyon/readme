# Sacha's README

Hello, I'm Sacha Guyon, a Senior Product Manager at GitLab, focusing on [Observabilty](https://about.gitlab.com/direction/analytics/observability/).

- [Linkedin](https://www.linkedin.com/in/sacha/)
- [GitLab](https://gitlab.com/sguyon)

# How I work

- I Live in Brooklyn, NY, with my wife and 2 year-old son.
- My timezone is [Easter Time](https://time.is/ET).
- I am originally from Paris, France.
- Before moving to the US, I lived in Shanghai, China, where I learned Mandarin and embraced a new work culture (and amazing food :).
- My background aligns the [classic PM venn diagram](https://medium.com/@rvirb/the-10-best-and-worst-venn-diagrams-explaining-product-management-f6006c82476c): I studied design and business, spent some years in web development and project management, and then transitioned ~~to the dark side~~ to product management.
- My typical workday runs from 9:30 AM to 6 PM. I sometimes take a mid-day break for a workout, currently enjoying Crossfit, F45 or Yoga.
- I enjoy lunches with good company around the city. If you're visiting New York, hit me up!
- I aim to consolidate my meetings into the morning, leaving my afternoons free for deep work.
- I’m an [INFP](https://eu.themyersbriggs.com/en/tools/MBTI/MBTI-personality-Types/INFP). I enjoy social interactions, but they can be draining, so I need time to recharge after too many back-to-back calls. Please book my time accordingly.
- I'm a proponent of [customer-centric approaches](https://www.inc.com/dave-bailey/why-you-need-to-follow-the-steve-jobs-method-and-w.html): "Start with the customer experience and work backwards to the technology", rather than the other way around.

# How to work with me

- Please @ me directly in issue comments as I use GitLab To-Do List as my inbox.
- I'm generally available for customer calls anytime, provided I get at least a day's notice.
- I enjoy working both synchronously and asynchronously, depending on the situation or project needs. I'm all for initial brainstorming sessions to explore ideas, be it over a call or a whiteboard. After those initial explorations, I like to dive into deep work and collaborate asynchronously to turn those ideas into concrete solutions.
- I value direct communication and candid feedback. If I seem too direct or interruptive, that's just my [French side](https://culturalatlas.sbs.com.au/french-culture/french-culture-communication#:~:text=Direct%20Communication%3A,than%20ill%2Dintentioned.) showing :) I'm actively working to adapt to different communication styles, so your feedback is always welcome.

Feel free to reach out to me for any collaborative opportunities or product discussions about Observability. Thank you for reading!

